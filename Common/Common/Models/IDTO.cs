using System;

namespace Common.Models
{
   public interface IDTO
   {
      string GetAsJson();
      object Get();
      Guid GetGUID();
   }
}
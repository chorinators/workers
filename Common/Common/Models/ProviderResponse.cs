﻿using System;

namespace Common.Models
{
   public class ProviderResponse
   {
      public string ProviderName { get; set; }
      public string Content { get; set; }
   }
}

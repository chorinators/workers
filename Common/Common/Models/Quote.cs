using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Common.Models
{
   public class Quote : IDTO
   {
      public Guid UUID { get; set; }
      public ICollection<ProviderResponse> Results { get; set; }

      public string GetAsJson()
      {
         return JsonConvert.SerializeObject(this);
      }

      public object Get()
      {
         return this;
      }

      public Guid GetGUID()
      {
         return UUID;
      }
   }
}

﻿using System;
using Newtonsoft.Json;

namespace Common.Models
{
   public class Request : IDTO
   {
      public Guid UUID { get; set; }
      public string Name { get; set; }

      public string GetAsJson()
      {
         return JsonConvert.SerializeObject(this);
      }
      public object Get()
      {
         return this;
      }

      public Guid GetGUID()
      {
         return UUID;
      }
   }
}

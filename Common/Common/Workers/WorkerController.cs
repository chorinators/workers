using System.Threading;
using System.Threading.Tasks;
using Common.Models;
using Microsoft.Extensions.Caching.Memory;

namespace Common.Workers
{
   public class WorkerController
   {
      private WorkerFactory _factory;
      private IMemoryCache _cache;

      public WorkerController(WorkerFactory factory)
      {
         _factory = factory;
         var options = new MemoryCacheOptions();
         _cache = new MemoryCache(options);
      }

      public IDTO ProcessWork(IDTO dto)
      {
         var worker = _factory.CreateWorker();

         return worker.ProcessWork(dto);
      }
   }
}
namespace Common.Workers
{
   public abstract class WorkerFactory
   {
      public abstract IWorker CreateWorker();
   }
}
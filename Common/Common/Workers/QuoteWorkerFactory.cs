using System;

namespace Common.Workers
{
   public class QuoteWorkerFactory : WorkerFactory
   {
      private string _prefix { get; set; }
      public QuoteWorkerFactory(string prefix)
      {
         _prefix = prefix;
      }

      public override IWorker CreateWorker()
      {
         var name = String.Format("{0}-{1}", _prefix, Guid.NewGuid().ToString());
         return new QuoteProviderWorker(name);
      }
   }
}
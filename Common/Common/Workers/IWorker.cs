using Common.Models;

namespace Common.Workers
{
   public interface IWorker
   {
      void Wait();
      void Stop();
      IDTO ProcessWork(IDTO input);

      string GetName();
   }
}

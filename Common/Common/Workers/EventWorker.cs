using System;
using System.Threading;
using Common.Models;

namespace Common.Workers
{
   public abstract class EventWorker : IWorker
   {
      private event Func<IDTO, IDTO> NewTask;
      private string Name { get; set; }
      private CancellationTokenSource _cancelSource;

      protected abstract IDTO ProcessWorkHandler(IDTO t);

      public EventWorker(string name)
      {
         NewTask += new Func<IDTO, IDTO>(ProcessWorkHandler);
         Name = name;
         _cancelSource = new CancellationTokenSource();
      }

      public IDTO ProcessWork(IDTO input)
      {
         return NewTask(input);
      }

      public void Wait()
      {
         _cancelSource.Token.WaitHandle.WaitOne();
      }

      public void Stop()
      {
         _cancelSource.Cancel();
      }

      public string GetName()
      {
         return Name;
      }
   }
}

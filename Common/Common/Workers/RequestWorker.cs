using System;
using System.Collections.Generic;
using System.Threading;
using Bogus;
using Common.Models;

namespace Common.Workers
{
   public class QuoteProviderWorker : EventWorker
   {
      public QuoteProviderWorker(string name) : base(name)
      {
      }

      protected override IDTO ProcessWorkHandler(IDTO request)
      {
         return GetQuote(request.Get() as Request);
      }

      private Quote GetQuote(Request request)
      {
         var quote = new Quote();
         quote.UUID = request.UUID;
         quote.Results = new List<ProviderResponse>();
         var testResults = new Faker<ProviderResponse>()
            .RuleFor(u => u.ProviderName, (f, u) => f.Company.CompanyName())
            .RuleFor(u => u.Content, (f, u) => f.Rant.Review());
         quote.Results.Add(testResults.Generate());
         quote.Results.Add(testResults.Generate());

         return quote;
      }
   }
}
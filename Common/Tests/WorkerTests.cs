﻿using System;
using Xunit;
using Common;
using System.Threading;
using System.Threading.Tasks;
using Common.Workers;
using System.Collections.Generic;
using Common.Models;

namespace Common.Tests
{
   public class WorkerTests
   {
      [Fact]
      public void TestOneWorker()
      {
         var factory = new QuoteWorkerFactory("test");
         var controller = new WorkerController(factory);
         var request = new Request
         {
            Name = "Test",
            UUID = Guid.NewGuid(),
         };

         var result = controller.ProcessWork(request);

         Console.WriteLine(result.GetAsJson());
         Assert.NotNull(result);
      }

      [Fact]
      public void TestMultipleWorkers()
      {
         var workers = new Dictionary<int, QuoteProviderWorker>();
         var tasks = new List<Task>();
         var rng = new Random(DateTime.Now.Millisecond);
         var maxWorkers = 10;

         // Create Workers
         for (int i = 0; i < maxWorkers; i++)
         {
            workers.Add(i, new QuoteProviderWorker("Worker " + i.ToString()));
         }

         // Start Workers
         foreach (var worker in workers)
         {
            var task = Task.Run(() =>
                  {
                     worker.Value.Wait();
                  });

            tasks.Add(task);
         }

         // Send Messages to random workers
         for (int i = 0; i < 20; i++)
         {
            var selector = rng.Next(maxWorkers);
            var worker = workers[selector];

            var request = new Request
            {
               UUID = Guid.NewGuid(),
               Name = i.ToString(),
            };

            var result = worker.ProcessWork(request);
            Console.WriteLine("Tarea {0} procesada por {1}", result.GetGUID(), worker.GetName());
         }

         // Stop All Workers
         foreach (var worker in workers)
         {
            worker.Value.Stop();
         }

         // Wait for all workers to stop
         Task.WaitAll(tasks.ToArray());
      }
   }
}
